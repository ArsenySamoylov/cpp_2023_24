#pragma once

#include <functional>

namespace cache {

template <typename Key_T>
class cache_interface {
    public:

    template <typename F>
    cache_interface(F load_f) : load_f_{load_f} {}

    virtual bool lookup_update (Key_T key) = 0;
    virtual ~cache_interface () = default;

    protected:
    std::function<Key_T(Key_T)> load_f_;
};
} // cache