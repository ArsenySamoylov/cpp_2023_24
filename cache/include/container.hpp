#pragma once

#include <cassert>
#include <iostream>
#include <unordered_map>

#include <source_location>
#include <string>

namespace fast_lookup_container {

#ifndef NDEBUG
bool Assert(bool condition, const std::string& message, 
            std::source_location loc = std::source_location::current()) {
    if (!condition) {
        std::cerr << loc.file_name() << ":" << loc.line() << " condition failed\n";
        std::cerr << message;
        return false;
        }
    
    return true;
}

#define DUMP_ASSERT(condition) do {             \
    if (!Assert(condition, #condition "\n")) {  \
        dump();                                \
        std::cerr << "Aborting\n";              \
        abort();                                \
        }                                       \
    if (!self_check()) {                        \
        std::cerr << "Full check failed\n"      \
                  << "Aborting\n";              \
        abort();                                \
    }                                           \
}while(0);

#define SELF_CHECK(container) do {               \
    if (!(container).self_check()) {               \
        std::cerr << "\nContainer '" #container  \
                  << "' in inconsistent state\n" \
                  << "Aborting\n";               \
        abort();                                 \
    }                                            \
}while(0);

#else
#define DUMP_ASSERT(condition)
#define SELF_CHECK(container)
#endif

template <typename ContainerT>
class container
    {
    public:
        using value_type = typename ContainerT::value_type;
        using reference  = typename ContainerT::reference;
        using size_type  = typename ContainerT::size_type;
        using iterator   = typename ContainerT::iterator;

        container (size_type size) : size_{size} {}

        bool is_full() const { 
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            return size_ <= container_.size(); 
            }
        
        bool contains (value_type key) const { 
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            return lookup_tbl_.contains(key);
            }

        iterator find (value_type key) {
            DUMP_ASSERT (lookup_tbl_.size() == container_.size());
            auto iter = lookup_tbl_.find(key);

            if (iter == lookup_tbl_.end())
                return end();

            return iter->second;
        }

        iterator begin() {
            DUMP_ASSERT (lookup_tbl_.size() == container_.size());
            return container_.begin();
        }

        iterator end() {
            DUMP_ASSERT (lookup_tbl_.size() == container_.size());
            return container_.end();
        }

        reference back() {
            DUMP_ASSERT (lookup_tbl_.size() == container_.size());
            return container_.back();
            }

        void pop_back() {
            DUMP_ASSERT (lookup_tbl_.size() == container_.size());

            lookup_tbl_.erase(container_.back());
            container_.pop_back();
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            }

        void push_front(value_type val) {
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            DUMP_ASSERT(!is_full());
            DUMP_ASSERT(contains(val) == false);
            
            container_.push_front(val);
            lookup_tbl_[val] = container_.begin();
            }

         void push_back(value_type val) {
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            DUMP_ASSERT(!is_full());
            DUMP_ASSERT(!contains(val));
            
            container_.push_back(val);
            lookup_tbl_[val] = std::prev(container_.end());
            }

        ContainerT& get_container() {
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            return container_;
        }

        void erase (value_type val) {
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            DUMP_ASSERT(contains(val));
            auto iter = lookup_tbl_.find(val);
            container_.erase(iter->second);
            lookup_tbl_.erase(iter);
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            }

        size_type size() const {
            DUMP_ASSERT(lookup_tbl_.size() == container_.size());
            return size_;
        }
    
    void dump(std::source_location loc = std::source_location::current()) const {
        print_location(loc, "\n--------------- CONTAINER DUMP: ----------------"); 
        std::cerr << "--------------------------------------------\n";

        std::cerr << "Size: " << size_ << std::endl;
        std::cerr << "Container (" << container_.size() << "): ";
        for (auto i_i : container_)
            std::cerr << i_i << " ";
        std::cerr << "\nMap (" << lookup_tbl_.size() << "): ";
        for (const auto& i_i : lookup_tbl_)
            std::cerr << "[" << i_i.first << ", *it:" << *i_i.second << "] ";
        std::cerr << std::endl;
        std::cerr << "--------------------------------------------\n";
        }

    void print_location (std::source_location loc, const std::string& message) const {
        std::cerr << message << std::endl;
        std::cerr << "From: " << loc.file_name() << ":" << loc.line() << std::endl;
    }
        
    bool self_check(std::source_location loc = std::source_location::current()) const {

        if (container_.size() != lookup_tbl_.size()) {
            print_location(loc, "SelfCheck: Container size not equal to LookUpTable size!");
            return false;
            }

        for (auto map : lookup_tbl_) {
            if (map.first != *map.second) {
                print_location(loc, "SelfCheck: Container element != LookUpTable iterator value");
                std::cerr << "(" << *map.second << " != " << map.first << ")" << std::endl;
                dump(loc);
                return false;
            }  
        }

        for (auto cnt : container_) {
            if (!lookup_tbl_.contains(cnt)) {
                print_location(loc, "SelfCheck: Container element not found in LookUpTable");
                std::cerr << "Can't find '" << cnt << "' in lookup table" << std::endl;
                return false;
            }
        }

        return true;  
    }

    protected:
        ContainerT container_;
        const size_type size_;
        std::unordered_map<value_type, iterator> lookup_tbl_;
    };

} // fast_lookup_container