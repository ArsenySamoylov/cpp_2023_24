#pragma once

#include <list>
#include <unordered_map>
#include <vector>
#include <deque>
#include <container.hpp>
#include <cache_interface.hpp>

namespace cache {

template <typename Key_T>
class occurrence_table final {
    public:
        using index_t = unsigned;
        using map_type = typename std::unordered_map<Key_T, std::deque<index_t>>;
        using mapped_type = typename map_type::mapped_type;

        occurrence_table (const std::vector<Key_T>& input)
            {
            for (auto index = 0U; index < input.size(); index++)
                occurrence_tbl_[input[index]].push_back(index);
            }

        mapped_type& at (const Key_T& key) { return occurrence_tbl_.at(key); }
        const mapped_type& at (const Key_T& key) const { return occurrence_tbl_.at(key); }

    private:
        map_type occurrence_tbl_;
    };

template <typename Key_T>
class idl_cache : public cache_interface<Key_T> {
    public:
        using input_type = typename std::vector<Key_T>;

        template <typename F>
        idl_cache (const input_type& input, size_t size, F load_f) :
                    cache_interface<Key_T> {load_f}, 
                    cache_{size}, 
                    input_ (input), current_input_id{0},
                    occurrence_ {input} 
                    {}

        bool lookup_update(const Key_T key_useless) override {
            auto key = input_[current_input_id++];
            assert(key == key_useless);
            
            assert(occurrence_.at(key).size() > 0);
            occurrence_.at(key).pop_front();
            
            if (cache_.contains(key))
                return true;

            if (!is_last_occurrence(key)) {
                load_f_(key);
                add_to_cache(key);
                }

            return false;
            }

    protected:
        using cache_interface<Key_T>::load_f_;

        void add_to_cache (Key_T key) {
                if (cache_.is_full())
                    cache_.erase(least_used());

                cache_.push_front(key);
                }    

        Key_T least_used () {
            size_t position = 0;
            Key_T least_used;

            for (auto key : cache_) {
                auto occurrence_queue = occurrence_.at(key);

                if (occurrence_queue.size() == 0)
                    return key;

                if (occurrence_queue.front() > position)
                    {
                    position = occurrence_queue.front();
                    least_used = key;
                    }
                }
            
            return least_used;
            }

        bool is_last_occurrence (Key_T key) const {
            auto occurrence_queue = occurrence_.at(key);
            return !occurrence_queue.size();
            }

        fast_lookup_container::container<std::list<Key_T>> cache_;
        const input_type input_;
        size_t current_input_id;
        occurrence_table<Key_T> occurrence_; 
    };

} // cache