#pragma once

#include <list>
#include <deque>
#include <container.hpp>
#include <cache_interface.hpp>

namespace cache {
template <typename Key_T>
class q2_cache : public cache_interface<Key_T> {
    public:
        static constexpr int hot_percentage = 25;
        static constexpr int in_percentage = 25;
        static constexpr int out_percentage = 50;
        static_assert(hot_percentage + in_percentage + out_percentage == 100,
                     "All percentage must be = 100%");

        static constexpr int min_size = 4;

        void print();

        template <typename F>
        q2_cache (size_t size, F load_f) :
            cache_interface<Key_T> {size >= min_size ? load_f : throw std::runtime_error("size < min_size")}, 
            in_  {(size * in_percentage) / 100},
            out_ {(size * out_percentage) / 100},
            hot_ {size - in_.size() - out_.size()} 
            {}

        bool lookup_update(const Key_T key) override {
            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);

            if (hot_.contains(key))
                return true;

            if (in_.contains(key))
                return true;

            if (out_.contains(key)) {
                move_to_hot (key);
                return true;
                }

            load_f_(key);
            add_to_in (key);
            return false;
            }

    protected:
        using cache_interface<Key_T>::load_f_;

        void add_to_in (const Key_T key) {
            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            
            if (in_.is_full())
                move_to_out();

            in_.push_front(key);

            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            }

        void move_to_out () {
            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            
            if (out_.is_full())
                {
                // Small optimization. If hot_ isn't full we can save it there
                if (!hot_.is_full())
                    hot_.push_back(out_.back());

                out_.pop_back();
                }

            out_.push_front(in_.back());
            in_.pop_back();

            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            }

        void move_to_hot (Key_T key) {
            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            
            if (hot_.is_full())
                hot_.pop_back();

            hot_.push_front(key);
            out_.erase(key);

            SELF_CHECK(in_);
            SELF_CHECK(out_);
            SELF_CHECK(hot_);
            }

        fast_lookup_container::container<std::list<Key_T>> in_;
        fast_lookup_container::container<std::list<Key_T>> out_;
        fast_lookup_container::container<std::list<Key_T>>  hot_;
    };


template <typename T>
void q2_cache<T>::print() {
    std::cerr << "-------------------------------------------\n";
    std::cerr << "\tIn:\n";
    in_.print();
    std::cerr << "\n\tOut:\n";
    out_.print();
    std::cerr << "\n\tHot:\n";
    hot_.print();
    std::cerr << "-------------------------------------------\n";
    }

} // cache