#pragma once 

#include <list>
#include <container.hpp>
#include <cache_interface.hpp>

namespace cache {

template <typename Key_T>
class lru_cache : public cache_interface<Key_T> {
    public:
        template <typename F>
        lru_cache (size_t size, const F load_f) : 
            cache_interface<Key_T>{load_f}, cache_{size} 
            {}

        bool lookup_update (const Key_T key)
            {
            if (!cache_.contains(key)) {
                load_f_(key);
                add_to_cache(key);
                return false;
                }

            auto it = cache_.find(key);

            if (it != cache_.begin())
                cache_.get_container().splice(cache_.begin(), cache_.get_container(), 
                                              it, std::next(it));

            return true;
            }   
    
      protected:  
        using cache_interface<Key_T>::load_f_;

        void add_to_cache (const Key_T key) {

            if (cache_.is_full())
                cache_.pop_back();
            
            cache_.push_front(key);
            }
        
        fast_lookup_container::container<std::list<Key_T>> cache_;
    };

} // cache