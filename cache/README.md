This is small library that provides different types of caches.

You can try on of the caches, following these instructions:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
cd example
./Example [OPTION]
<n_elements-cache_size-input>
```

Possible options:

    -q2 - to ran 2Q cache