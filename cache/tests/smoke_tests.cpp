#include <gtest/gtest.h>

#include <lru_cache.hpp>
#include <idl_cache.hpp>
#include <q2_cache.hpp>

using page_id = int;

namespace {
page_id slow_load (page_id id) { return id; } 
}

using namespace cache;

//////////////////////////////////////////////////////
// Smoke Tests
//////////////////////////////////////////////////////
TEST(SmokeTest, Idl_Cache) {
    idl_cache<page_id> test {std::vector<page_id>{1, 2, 3, 4, 5}, 1, slow_load};
    test.lookup_update(1);
    }

TEST(SmokeTest, LRU_Cache) {
    lru_cache<page_id> test {1, slow_load};
    test.lookup_update(0);
    }

TEST(SmokeTest, Q2_Cache) {
    q2_cache<page_id> test {q2_cache<page_id>::min_size, slow_load};
    test.lookup_update(0);
    }