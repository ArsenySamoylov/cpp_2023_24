#include <gtest/gtest.h>

#include <lru_cache.hpp>
#include <idl_cache.hpp>
#include <q2_cache.hpp>

using page_id = int;

namespace {
page_id slow_load (page_id id) { return id; } 
}

using namespace cache;

//////////////////////////////////////////////////////
// Hit Tests
//////////////////////////////////////////////////////
struct test_input {
    std::vector<page_id> input;
    size_t size;

    struct {
        const int idl_hits;
        const int q2_hits;
        };
    };

const test_input TESTS[] = {
                {{1, 2, 3, 4, 1, 2, 5, 5, 2, 4, 3, 4, 1},              4, {7, 6}},
                {{1, 2, 1, 2, 1, 2, 1, 2, 1, 2},                       4, {8, 8}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},                      4, {0, 0}},
                {{1, 2, 3, 4, 4, 1, 2, 10, 7, 11},                     6, {3, 3}},
                {{1, 2, 2, 1, 5, 4, 2, 1, 10, 6, 7, 8, 10, 11, 12, 5}, 8, {6, 5}},
                {{8, 4, 1, 7, 1, 2, 3, 8, 7, 9, 0, 1, 0, 6, 10, 3,
                0, 8, 3, 6, 2, 2, 7, 6, 7, 4, 10, 3, 8, 3, 6, 7, 4, 
                9, 8, 8, 5, 0, 3, 7, 3, 1, 9, 9, 7, 5, 9, 9, 9, 2, 4, 
                8, 1, 4, 10, 8, 5, 0, 9, 5, 0, 5, 6, 2, 4, 1, 9, 10, 
                7, 2, 7, 10, 10, 4, 0, 8, 9, 7, 3, 6, 4, 1, 5, 8, 8, 
                2, 7, 3, 3, 3, 2,7, 4, 2, 9, 7, 7, 1, 0, 5},           5, {63, 37}},
                {{1, 2, 3, 4, 1, 2, 5, 1, 2, 4, 3, 4},                 4, {7,5}}                            
                            };

class HitsTestParametrizedTestFixture : public ::testing::TestWithParam<test_input> {
    protected:

    template <typename Cache_T>
    unsigned test_cache (const std::vector<page_id>& input, Cache_T cache) {
        auto hits = 0u;
        for (auto key : input)
            {
            if (cache.lookup_update(key))
                hits++;
            }

        return hits;
        }

    void run_test (const test_input& test)
        {
        const size_t size = test.size;

        idl_cache<page_id> cache {test.input, size, slow_load};
        q2_cache<page_id> cache_q2 {size, slow_load};
        
        EXPECT_EQ(test_cache(test.input, cache), test.idl_hits);
        EXPECT_EQ(test_cache(test.input, cache_q2), test.q2_hits);
        }
    };

TEST_P(HitsTestParametrizedTestFixture, 0) {
    run_test(GetParam());
    }

INSTANTIATE_TEST_SUITE_P(
    BaseTest,
    HitsTestParametrizedTestFixture,
    ::testing::Values(  TESTS[0], TESTS[1],
                        TESTS[2], TESTS[3],
                        TESTS[4], TESTS[5],
                        TESTS[6])
);