#include <idl_cache.hpp>
#include <q2_cache.hpp>

#include <cassert>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <vector>

namespace {
using page_id = int;
auto dummy(page_id key) { return key; }

template <typename Cache_T>
int test_cache (const std::vector<page_id>& input, Cache_T cache) 
    {
    auto hits = 0;
    for (auto key : input)
        if (cache.lookup_update(key))
            hits++;
    return hits;
    }
} // namespace

int main(int argc, const char* argv[])
    {
    if (argc > 1 && std::strcmp(argv[1], "-q2") != 0) {
        std::cout << "Unknown parameter '" << argv[1] << "'\n";
        return -1;
    }
    const bool use_q2 = argc > 1;

    std::size_t cnt; 
    std::size_t size;
    std::cin >> size >> cnt;

    std::vector<page_id> input(cnt);
    for (auto i = 0LU; i < cnt; i++) {
        page_id key;
        std::cin >> key;
        assert(std::cin);

        input[i] = key;
    }
    
    auto hits = 0;
    if (use_q2) {
        try {
            hits = test_cache(input, cache::q2_cache<page_id> {size, dummy});
        }
        catch (const std::runtime_error&) {
            hits = 0;
        }
    }
    else
        hits = test_cache(input, cache::idl_cache<page_id> {input, size, dummy});

    std::cout << hits << std::endl;
    }
